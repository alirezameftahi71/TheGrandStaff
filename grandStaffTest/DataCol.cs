﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace grandStaffTest
{
    public static class DataCol
    {
        public static int countDown { get; set; } = 20;
        public static string nameTB { get; set; } = "unknownTextBox";
        public static int negPoint { get; set; } = 5;
        public static int posPoint { get; set; } = 10;
        public static int counterPoint { get; set; } = 20;
    }
}
