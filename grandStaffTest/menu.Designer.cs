﻿namespace grandStaffTest
{
    partial class menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menu));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAbout = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnHighScore = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lbHighScore = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.newHighRecord = new System.Windows.Forms.Label();
            this.btn_back = new System.Windows.Forms.Button();
            this.lbNeg = new System.Windows.Forms.Label();
            this.lbPos = new System.Windows.Forms.Label();
            this.lbScore = new System.Windows.Forms.Label();
            this.btn_stop = new System.Windows.Forms.Button();
            this.lbCountD = new System.Windows.Forms.Label();
            this.btn_start = new System.Windows.Forms.Button();
            this.bsS_A = new System.Windows.Forms.TextBox();
            this.bsS_C = new System.Windows.Forms.TextBox();
            this.bsS_G = new System.Windows.Forms.TextBox();
            this.bsL_G = new System.Windows.Forms.TextBox();
            this.bsL_B = new System.Windows.Forms.TextBox();
            this.bsL_D = new System.Windows.Forms.TextBox();
            this.bsL_F = new System.Windows.Forms.TextBox();
            this.bsS_E = new System.Windows.Forms.TextBox();
            this.bsL_A = new System.Windows.Forms.TextBox();
            this.tbL_E = new System.Windows.Forms.TextBox();
            this.tbL_G = new System.Windows.Forms.TextBox();
            this.tbL_B = new System.Windows.Forms.TextBox();
            this.tbL_D = new System.Windows.Forms.TextBox();
            this.tbL_F = new System.Windows.Forms.TextBox();
            this.tbS_F = new System.Windows.Forms.TextBox();
            this.tbS_A = new System.Windows.Forms.TextBox();
            this.tbS_C = new System.Windows.Forms.TextBox();
            this.tbS_E = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.btnOkName = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(-21, -79);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1768, 941);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 43);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage1.Size = new System.Drawing.Size(1760, 894);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Maroon;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.btnAbout);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Controls.Add(this.btnHighScore);
            this.panel1.Controls.Add(this.btnPlay);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.Color.Maroon;
            this.panel1.Location = new System.Drawing.Point(8, 166);
            this.panel1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1744, 721);
            this.panel1.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(811, 33);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(880, 634);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // btnAbout
            // 
            this.btnAbout.BackColor = System.Drawing.Color.IndianRed;
            this.btnAbout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAbout.FlatAppearance.BorderSize = 0;
            this.btnAbout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbout.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbout.ForeColor = System.Drawing.Color.White;
            this.btnAbout.Location = new System.Drawing.Point(179, 365);
            this.btnAbout.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(461, 124);
            this.btnAbout.TabIndex = 2;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = false;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.IndianRed;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(179, 503);
            this.btnExit.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(461, 124);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnHighScore
            // 
            this.btnHighScore.BackColor = System.Drawing.Color.IndianRed;
            this.btnHighScore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHighScore.FlatAppearance.BorderSize = 0;
            this.btnHighScore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHighScore.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHighScore.ForeColor = System.Drawing.Color.White;
            this.btnHighScore.Location = new System.Drawing.Point(179, 227);
            this.btnHighScore.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnHighScore.Name = "btnHighScore";
            this.btnHighScore.Size = new System.Drawing.Size(461, 124);
            this.btnHighScore.TabIndex = 1;
            this.btnHighScore.Text = "Highest Score";
            this.btnHighScore.UseVisualStyleBackColor = false;
            this.btnHighScore.Click += new System.EventHandler(this.btnHighScore_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.BackColor = System.Drawing.Color.IndianRed;
            this.btnPlay.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPlay.FlatAppearance.BorderSize = 0;
            this.btnPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlay.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlay.ForeColor = System.Drawing.Color.White;
            this.btnPlay.Location = new System.Drawing.Point(179, 88);
            this.btnPlay.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(461, 124);
            this.btnPlay.TabIndex = 0;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = false;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Maroon;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(8, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1744, 159);
            this.label1.TabIndex = 3;
            this.label1.Text = "The Grand Staff Test";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Maroon;
            this.tabPage2.Controls.Add(this.lbHighScore);
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Location = new System.Drawing.Point(4, 43);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage2.Size = new System.Drawing.Size(1760, 894);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            // 
            // lbHighScore
            // 
            this.lbHighScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHighScore.ForeColor = System.Drawing.Color.White;
            this.lbHighScore.Location = new System.Drawing.Point(736, 378);
            this.lbHighScore.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbHighScore.Name = "lbHighScore";
            this.lbHighScore.Size = new System.Drawing.Size(653, 174);
            this.lbHighScore.TabIndex = 5;
            this.lbHighScore.Text = "0";
            this.lbHighScore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(34, 187);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(659, 599);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.IndianRed;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(1435, 378);
            this.button2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(200, 69);
            this.button2.TabIndex = 2;
            this.button2.Text = "REST";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.IndianRed;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(1435, 483);
            this.button1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 69);
            this.button1.TabIndex = 2;
            this.button1.Text = "BACK";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(8, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1744, 112);
            this.label2.TabIndex = 1;
            this.label2.Text = "Highest Score";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Maroon;
            this.tabPage3.Controls.Add(this.newHighRecord);
            this.tabPage3.Controls.Add(this.btn_back);
            this.tabPage3.Controls.Add(this.lbNeg);
            this.tabPage3.Controls.Add(this.lbPos);
            this.tabPage3.Controls.Add(this.lbScore);
            this.tabPage3.Controls.Add(this.btn_stop);
            this.tabPage3.Controls.Add(this.lbCountD);
            this.tabPage3.Controls.Add(this.btn_start);
            this.tabPage3.Controls.Add(this.bsS_A);
            this.tabPage3.Controls.Add(this.bsS_C);
            this.tabPage3.Controls.Add(this.bsS_G);
            this.tabPage3.Controls.Add(this.bsL_G);
            this.tabPage3.Controls.Add(this.bsL_B);
            this.tabPage3.Controls.Add(this.bsL_D);
            this.tabPage3.Controls.Add(this.bsL_F);
            this.tabPage3.Controls.Add(this.bsS_E);
            this.tabPage3.Controls.Add(this.bsL_A);
            this.tabPage3.Controls.Add(this.tbL_E);
            this.tabPage3.Controls.Add(this.tbL_G);
            this.tabPage3.Controls.Add(this.tbL_B);
            this.tabPage3.Controls.Add(this.tbL_D);
            this.tabPage3.Controls.Add(this.tbL_F);
            this.tabPage3.Controls.Add(this.tbS_F);
            this.tabPage3.Controls.Add(this.tbS_A);
            this.tabPage3.Controls.Add(this.tbS_C);
            this.tabPage3.Controls.Add(this.tbS_E);
            this.tabPage3.Controls.Add(this.pictureBox3);
            this.tabPage3.Location = new System.Drawing.Point(4, 43);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage3.Size = new System.Drawing.Size(1760, 894);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            // 
            // newHighRecord
            // 
            this.newHighRecord.AutoSize = true;
            this.newHighRecord.ForeColor = System.Drawing.Color.White;
            this.newHighRecord.Location = new System.Drawing.Point(1315, 508);
            this.newHighRecord.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.newHighRecord.Name = "newHighRecord";
            this.newHighRecord.Size = new System.Drawing.Size(271, 32);
            this.newHighRecord.TabIndex = 51;
            this.newHighRecord.Text = "NEW HIGHSCORE! ";
            this.newHighRecord.Visible = false;
            // 
            // btn_back
            // 
            this.btn_back.BackColor = System.Drawing.Color.IndianRed;
            this.btn_back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_back.FlatAppearance.BorderSize = 0;
            this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_back.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_back.ForeColor = System.Drawing.Color.White;
            this.btn_back.Location = new System.Drawing.Point(1243, 630);
            this.btn_back.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(192, 91);
            this.btn_back.TabIndex = 50;
            this.btn_back.Text = "Quit";
            this.btn_back.UseVisualStyleBackColor = false;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // lbNeg
            // 
            this.lbNeg.AutoSize = true;
            this.lbNeg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNeg.ForeColor = System.Drawing.Color.Red;
            this.lbNeg.Location = new System.Drawing.Point(1389, 375);
            this.lbNeg.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbNeg.Name = "lbNeg";
            this.lbNeg.Size = new System.Drawing.Size(101, 36);
            this.lbNeg.TabIndex = 49;
            this.lbNeg.Text = "label2";
            // 
            // lbPos
            // 
            this.lbPos.AutoSize = true;
            this.lbPos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lbPos.Location = new System.Drawing.Point(1389, 375);
            this.lbPos.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbPos.Name = "lbPos";
            this.lbPos.Size = new System.Drawing.Size(101, 36);
            this.lbPos.TabIndex = 48;
            this.lbPos.Text = "label1";
            // 
            // lbScore
            // 
            this.lbScore.AutoSize = true;
            this.lbScore.ForeColor = System.Drawing.Color.White;
            this.lbScore.Location = new System.Drawing.Point(1235, 167);
            this.lbScore.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbScore.Name = "lbScore";
            this.lbScore.Size = new System.Drawing.Size(89, 32);
            this.lbScore.TabIndex = 46;
            this.lbScore.Text = "Score";
            // 
            // btn_stop
            // 
            this.btn_stop.BackColor = System.Drawing.Color.IndianRed;
            this.btn_stop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_stop.FlatAppearance.BorderSize = 0;
            this.btn_stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_stop.ForeColor = System.Drawing.Color.White;
            this.btn_stop.Location = new System.Drawing.Point(1475, 630);
            this.btn_stop.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(192, 91);
            this.btn_stop.TabIndex = 44;
            this.btn_stop.Text = "Stop";
            this.btn_stop.UseVisualStyleBackColor = false;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // lbCountD
            // 
            this.lbCountD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCountD.ForeColor = System.Drawing.Color.White;
            this.lbCountD.Location = new System.Drawing.Point(1341, 325);
            this.lbCountD.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.lbCountD.Name = "lbCountD";
            this.lbCountD.Size = new System.Drawing.Size(227, 48);
            this.lbCountD.TabIndex = 43;
            this.lbCountD.Text = "Count Down";
            this.lbCountD.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_start
            // 
            this.btn_start.BackColor = System.Drawing.Color.IndianRed;
            this.btn_start.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_start.FlatAppearance.BorderSize = 0;
            this.btn_start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_start.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_start.ForeColor = System.Drawing.Color.White;
            this.btn_start.Location = new System.Drawing.Point(1475, 630);
            this.btn_start.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(192, 91);
            this.btn_start.TabIndex = 41;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = false;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // bsS_A
            // 
            this.bsS_A.BackColor = System.Drawing.Color.IndianRed;
            this.bsS_A.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bsS_A.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.bsS_A.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsS_A.ForeColor = System.Drawing.Color.White;
            this.bsS_A.Location = new System.Drawing.Point(459, 620);
            this.bsS_A.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bsS_A.MaxLength = 1;
            this.bsS_A.Name = "bsS_A";
            this.bsS_A.Size = new System.Drawing.Size(123, 32);
            this.bsS_A.TabIndex = 34;
            this.bsS_A.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bsS_A.TextChanged += new System.EventHandler(this.bsS_A_TextChanged);
            // 
            // bsS_C
            // 
            this.bsS_C.BackColor = System.Drawing.Color.IndianRed;
            this.bsS_C.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bsS_C.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.bsS_C.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsS_C.ForeColor = System.Drawing.Color.White;
            this.bsS_C.Location = new System.Drawing.Point(459, 582);
            this.bsS_C.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bsS_C.MaxLength = 1;
            this.bsS_C.Name = "bsS_C";
            this.bsS_C.Size = new System.Drawing.Size(123, 32);
            this.bsS_C.TabIndex = 35;
            this.bsS_C.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bsS_C.TextChanged += new System.EventHandler(this.bsS_C_TextChanged);
            // 
            // bsS_G
            // 
            this.bsS_G.BackColor = System.Drawing.Color.IndianRed;
            this.bsS_G.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bsS_G.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.bsS_G.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsS_G.ForeColor = System.Drawing.Color.White;
            this.bsS_G.Location = new System.Drawing.Point(459, 508);
            this.bsS_G.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bsS_G.MaxLength = 1;
            this.bsS_G.Name = "bsS_G";
            this.bsS_G.Size = new System.Drawing.Size(123, 32);
            this.bsS_G.TabIndex = 37;
            this.bsS_G.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bsS_G.TextChanged += new System.EventHandler(this.bsS_G_TextChanged);
            // 
            // bsL_G
            // 
            this.bsL_G.BackColor = System.Drawing.Color.IndianRed;
            this.bsL_G.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bsL_G.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.bsL_G.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsL_G.ForeColor = System.Drawing.Color.White;
            this.bsL_G.Location = new System.Drawing.Point(808, 639);
            this.bsL_G.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bsL_G.MaxLength = 1;
            this.bsL_G.Name = "bsL_G";
            this.bsL_G.Size = new System.Drawing.Size(120, 32);
            this.bsL_G.TabIndex = 38;
            this.bsL_G.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bsL_G.TextChanged += new System.EventHandler(this.bsL_G_TextChanged);
            // 
            // bsL_B
            // 
            this.bsL_B.BackColor = System.Drawing.Color.IndianRed;
            this.bsL_B.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bsL_B.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.bsL_B.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsL_B.ForeColor = System.Drawing.Color.White;
            this.bsL_B.Location = new System.Drawing.Point(808, 601);
            this.bsL_B.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bsL_B.MaxLength = 1;
            this.bsL_B.Name = "bsL_B";
            this.bsL_B.Size = new System.Drawing.Size(120, 32);
            this.bsL_B.TabIndex = 39;
            this.bsL_B.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bsL_B.TextChanged += new System.EventHandler(this.bsL_B_TextChanged);
            // 
            // bsL_D
            // 
            this.bsL_D.BackColor = System.Drawing.Color.IndianRed;
            this.bsL_D.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bsL_D.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.bsL_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsL_D.ForeColor = System.Drawing.Color.White;
            this.bsL_D.Location = new System.Drawing.Point(808, 563);
            this.bsL_D.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bsL_D.MaxLength = 1;
            this.bsL_D.Name = "bsL_D";
            this.bsL_D.Size = new System.Drawing.Size(120, 32);
            this.bsL_D.TabIndex = 40;
            this.bsL_D.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bsL_D.TextChanged += new System.EventHandler(this.bsL_D_TextChanged);
            // 
            // bsL_F
            // 
            this.bsL_F.BackColor = System.Drawing.Color.IndianRed;
            this.bsL_F.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bsL_F.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.bsL_F.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsL_F.ForeColor = System.Drawing.Color.White;
            this.bsL_F.Location = new System.Drawing.Point(808, 525);
            this.bsL_F.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bsL_F.MaxLength = 1;
            this.bsL_F.Name = "bsL_F";
            this.bsL_F.Size = new System.Drawing.Size(120, 32);
            this.bsL_F.TabIndex = 42;
            this.bsL_F.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bsL_F.TextChanged += new System.EventHandler(this.bsL_F_TextChanged);
            // 
            // bsS_E
            // 
            this.bsS_E.BackColor = System.Drawing.Color.IndianRed;
            this.bsS_E.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bsS_E.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.bsS_E.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsS_E.ForeColor = System.Drawing.Color.White;
            this.bsS_E.Location = new System.Drawing.Point(459, 544);
            this.bsS_E.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bsS_E.MaxLength = 1;
            this.bsS_E.Name = "bsS_E";
            this.bsS_E.Size = new System.Drawing.Size(123, 32);
            this.bsS_E.TabIndex = 36;
            this.bsS_E.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bsS_E.TextChanged += new System.EventHandler(this.bsS_E_TextChanged);
            // 
            // bsL_A
            // 
            this.bsL_A.BackColor = System.Drawing.Color.IndianRed;
            this.bsL_A.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bsL_A.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.bsL_A.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsL_A.ForeColor = System.Drawing.Color.White;
            this.bsL_A.Location = new System.Drawing.Point(808, 487);
            this.bsL_A.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.bsL_A.MaxLength = 1;
            this.bsL_A.Name = "bsL_A";
            this.bsL_A.Size = new System.Drawing.Size(120, 32);
            this.bsL_A.TabIndex = 45;
            this.bsL_A.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.bsL_A.TextChanged += new System.EventHandler(this.bsL_A_TextChanged);
            // 
            // tbL_E
            // 
            this.tbL_E.BackColor = System.Drawing.Color.IndianRed;
            this.tbL_E.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbL_E.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbL_E.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbL_E.ForeColor = System.Drawing.Color.White;
            this.tbL_E.Location = new System.Drawing.Point(808, 375);
            this.tbL_E.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbL_E.MaxLength = 1;
            this.tbL_E.Name = "tbL_E";
            this.tbL_E.Size = new System.Drawing.Size(120, 32);
            this.tbL_E.TabIndex = 29;
            this.tbL_E.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbL_E.TextChanged += new System.EventHandler(this.tbL_E_TextChanged);
            // 
            // tbL_G
            // 
            this.tbL_G.BackColor = System.Drawing.Color.IndianRed;
            this.tbL_G.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbL_G.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbL_G.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbL_G.ForeColor = System.Drawing.Color.White;
            this.tbL_G.Location = new System.Drawing.Point(808, 337);
            this.tbL_G.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbL_G.MaxLength = 1;
            this.tbL_G.Name = "tbL_G";
            this.tbL_G.Size = new System.Drawing.Size(120, 32);
            this.tbL_G.TabIndex = 30;
            this.tbL_G.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbL_G.TextChanged += new System.EventHandler(this.tbL_G_TextChanged);
            // 
            // tbL_B
            // 
            this.tbL_B.BackColor = System.Drawing.Color.IndianRed;
            this.tbL_B.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbL_B.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbL_B.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbL_B.ForeColor = System.Drawing.Color.White;
            this.tbL_B.Location = new System.Drawing.Point(808, 298);
            this.tbL_B.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbL_B.MaxLength = 1;
            this.tbL_B.Name = "tbL_B";
            this.tbL_B.Size = new System.Drawing.Size(120, 32);
            this.tbL_B.TabIndex = 31;
            this.tbL_B.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbL_B.TextChanged += new System.EventHandler(this.tbL_B_TextChanged);
            // 
            // tbL_D
            // 
            this.tbL_D.BackColor = System.Drawing.Color.IndianRed;
            this.tbL_D.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbL_D.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbL_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbL_D.ForeColor = System.Drawing.Color.White;
            this.tbL_D.Location = new System.Drawing.Point(808, 260);
            this.tbL_D.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbL_D.MaxLength = 1;
            this.tbL_D.Name = "tbL_D";
            this.tbL_D.Size = new System.Drawing.Size(120, 32);
            this.tbL_D.TabIndex = 32;
            this.tbL_D.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbL_D.TextChanged += new System.EventHandler(this.tbL_D_TextChanged);
            // 
            // tbL_F
            // 
            this.tbL_F.BackColor = System.Drawing.Color.IndianRed;
            this.tbL_F.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbL_F.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbL_F.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbL_F.ForeColor = System.Drawing.Color.White;
            this.tbL_F.Location = new System.Drawing.Point(808, 222);
            this.tbL_F.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbL_F.MaxLength = 1;
            this.tbL_F.Name = "tbL_F";
            this.tbL_F.Size = new System.Drawing.Size(120, 32);
            this.tbL_F.TabIndex = 33;
            this.tbL_F.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbL_F.TextChanged += new System.EventHandler(this.tbL_F_TextChanged);
            // 
            // tbS_F
            // 
            this.tbS_F.BackColor = System.Drawing.Color.IndianRed;
            this.tbS_F.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbS_F.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbS_F.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbS_F.ForeColor = System.Drawing.Color.White;
            this.tbS_F.Location = new System.Drawing.Point(459, 356);
            this.tbS_F.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbS_F.MaxLength = 1;
            this.tbS_F.Name = "tbS_F";
            this.tbS_F.Size = new System.Drawing.Size(123, 32);
            this.tbS_F.TabIndex = 25;
            this.tbS_F.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbS_F.TextChanged += new System.EventHandler(this.tbS_F_TextChanged);
            // 
            // tbS_A
            // 
            this.tbS_A.BackColor = System.Drawing.Color.IndianRed;
            this.tbS_A.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbS_A.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbS_A.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbS_A.ForeColor = System.Drawing.Color.White;
            this.tbS_A.Location = new System.Drawing.Point(459, 318);
            this.tbS_A.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbS_A.MaxLength = 1;
            this.tbS_A.Name = "tbS_A";
            this.tbS_A.Size = new System.Drawing.Size(123, 32);
            this.tbS_A.TabIndex = 26;
            this.tbS_A.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbS_A.TextChanged += new System.EventHandler(this.tbS_A_TextChanged);
            // 
            // tbS_C
            // 
            this.tbS_C.BackColor = System.Drawing.Color.IndianRed;
            this.tbS_C.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbS_C.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbS_C.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbS_C.ForeColor = System.Drawing.Color.White;
            this.tbS_C.Location = new System.Drawing.Point(459, 279);
            this.tbS_C.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbS_C.MaxLength = 1;
            this.tbS_C.Name = "tbS_C";
            this.tbS_C.Size = new System.Drawing.Size(123, 32);
            this.tbS_C.TabIndex = 27;
            this.tbS_C.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbS_C.TextChanged += new System.EventHandler(this.tbS_C_TextChanged);
            // 
            // tbS_E
            // 
            this.tbS_E.BackColor = System.Drawing.Color.IndianRed;
            this.tbS_E.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbS_E.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbS_E.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbS_E.ForeColor = System.Drawing.Color.White;
            this.tbS_E.Location = new System.Drawing.Point(459, 241);
            this.tbS_E.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tbS_E.MaxLength = 1;
            this.tbS_E.Name = "tbS_E";
            this.tbS_E.Size = new System.Drawing.Size(123, 32);
            this.tbS_E.TabIndex = 28;
            this.tbS_E.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbS_E.TextChanged += new System.EventHandler(this.tbS_E_TextChanged);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(37, 165);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(1149, 556);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 24;
            this.pictureBox3.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Maroon;
            this.tabPage4.Controls.Add(this.pictureBox4);
            this.tabPage4.Controls.Add(this.radioButton3);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Controls.Add(this.radioButton2);
            this.tabPage4.Controls.Add(this.radioButton1);
            this.tabPage4.Controls.Add(this.btnOkName);
            this.tabPage4.Location = new System.Drawing.Point(4, 43);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.tabPage4.Size = new System.Drawing.Size(1760, 894);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(1173, 185);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(453, 215);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 10;
            this.pictureBox4.TabStop = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.FlatAppearance.BorderSize = 0;
            this.radioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.radioButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton3.ForeColor = System.Drawing.Color.White;
            this.radioButton3.Location = new System.Drawing.Point(552, 526);
            this.radioButton3.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(153, 56);
            this.radioButton3.TabIndex = 9;
            this.radioButton3.Text = "Hard";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(163, 276);
            this.label3.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(267, 62);
            this.label3.TabIndex = 8;
            this.label3.Text = "Difficulty : ";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.FlatAppearance.BorderSize = 0;
            this.radioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.radioButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.ForeColor = System.Drawing.Color.White;
            this.radioButton2.Location = new System.Drawing.Point(552, 397);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(214, 56);
            this.radioButton2.TabIndex = 6;
            this.radioButton2.Text = "Medium";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.FlatAppearance.BorderSize = 0;
            this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.ForeColor = System.Drawing.Color.White;
            this.radioButton1.Location = new System.Drawing.Point(552, 276);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(155, 56);
            this.radioButton1.TabIndex = 7;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Easy";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // btnOkName
            // 
            this.btnOkName.BackColor = System.Drawing.Color.IndianRed;
            this.btnOkName.FlatAppearance.BorderSize = 0;
            this.btnOkName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOkName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOkName.ForeColor = System.Drawing.Color.White;
            this.btnOkName.Location = new System.Drawing.Point(1283, 633);
            this.btnOkName.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.btnOkName.Name = "btnOkName";
            this.btnOkName.Size = new System.Drawing.Size(251, 95);
            this.btnOkName.TabIndex = 1;
            this.btnOkName.Text = "OK";
            this.btnOkName.UseVisualStyleBackColor = false;
            this.btnOkName.Click += new System.EventHandler(this.btnOkName_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // menu
            // 
            this.AcceptButton = this.btnOkName;
            this.AutoScaleDimensions = new System.Drawing.SizeF(240F, 240F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(1699, 835);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.MaximizeBox = false;
            this.Name = "menu";
            this.Text = "Grand Staff Test";
            this.Load += new System.EventHandler(this.menu_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnHighScore;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.Label lbNeg;
        private System.Windows.Forms.Label lbPos;
        private System.Windows.Forms.Label lbScore;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Label lbCountD;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.TextBox bsS_A;
        private System.Windows.Forms.TextBox bsS_C;
        private System.Windows.Forms.TextBox bsS_G;
        private System.Windows.Forms.TextBox bsL_G;
        private System.Windows.Forms.TextBox bsL_B;
        private System.Windows.Forms.TextBox bsL_D;
        private System.Windows.Forms.TextBox bsL_F;
        private System.Windows.Forms.TextBox bsS_E;
        private System.Windows.Forms.TextBox bsL_A;
        private System.Windows.Forms.TextBox tbL_E;
        private System.Windows.Forms.TextBox tbL_G;
        private System.Windows.Forms.TextBox tbL_B;
        private System.Windows.Forms.TextBox tbL_D;
        private System.Windows.Forms.TextBox tbL_F;
        private System.Windows.Forms.TextBox tbS_F;
        private System.Windows.Forms.TextBox tbS_A;
        private System.Windows.Forms.TextBox tbS_C;
        private System.Windows.Forms.TextBox tbS_E;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnOkName;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label lbHighScore;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label newHighRecord;
    }
}