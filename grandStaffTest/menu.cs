﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace grandStaffTest
{
    public partial class menu : Form
    {
        public menu()
        {
            InitializeComponent();
        }

        Random rnd = new Random();
        public void genRand()
        {
            disableAllTbox();
            int randNum = rnd.Next(1, 19);
            switch (randNum)
            {
                case 1:
                    tbS_E.Visible = true;
                    tbS_E.Focus();
                    DataCol.nameTB = tbS_E.Name;
                    break;
                case 2:
                    tbS_C.Visible = true;
                    tbS_C.Focus();
                    DataCol.nameTB = tbS_C.Name;
                    break;
                case 3:
                    tbS_A.Visible = true;
                    tbS_A.Focus();
                    DataCol.nameTB = tbS_A.Name;
                    break;
                case 4:
                    tbS_F.Visible = true;
                    tbS_F.Focus();
                    DataCol.nameTB = tbS_F.Name;
                    break;
                case 5:
                    tbL_F.Visible = true;
                    tbL_F.Focus();
                    DataCol.nameTB = tbL_F.Name;
                    break;
                case 6:
                    tbL_D.Visible = true;
                    tbL_D.Focus();
                    DataCol.nameTB = tbL_D.Name;
                    break;
                case 7:
                    tbL_B.Visible = true;
                    tbL_B.Focus();
                    DataCol.nameTB = tbL_B.Name;
                    break;
                case 8:
                    tbL_G.Visible = true;
                    tbL_G.Focus();
                    DataCol.nameTB = tbL_G.Name;
                    break;
                case 9:
                    tbL_E.Visible = true;
                    tbL_E.Focus();
                    DataCol.nameTB = tbL_E.Name;
                    break;
                case 10:
                    bsL_G.Visible = true;
                    bsL_G.Focus();
                    DataCol.nameTB = bsL_G.Name;
                    break;
                case 11:
                    bsL_B.Visible = true;
                    bsL_B.Focus();
                    DataCol.nameTB = bsL_B.Name;
                    break;
                case 12:
                    bsL_D.Visible = true;
                    bsL_D.Focus();
                    DataCol.nameTB = bsL_D.Name;
                    break;
                case 13:
                    bsL_F.Visible = true;
                    bsL_F.Focus();
                    DataCol.nameTB = bsL_F.Name;
                    break;
                case 14:
                    bsL_A.Visible = true;
                    bsL_A.Focus();
                    DataCol.nameTB = bsL_A.Name;
                    break;
                case 15:
                    bsS_A.Visible = true;
                    bsS_A.Focus();
                    DataCol.nameTB = bsS_A.Name;
                    break;
                case 16:
                    bsS_C.Visible = true;
                    bsS_C.Focus();
                    DataCol.nameTB = bsS_C.Name;
                    break;
                case 17:
                    bsS_E.Visible = true;
                    bsS_E.Focus();
                    DataCol.nameTB = bsS_E.Name;
                    break;
                case 18:
                    bsS_G.Visible = true;
                    bsS_G.Focus();
                    DataCol.nameTB = bsS_G.Name;
                    break;
                default:
                    MessageBox.Show("Something Happened!");
                    break;
            }
        }

        public void startTest()
        {
            disableAllTbox();
            lbScore.Text = Player.score.ToString();
            genRand();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 3;
            radioButton1.Checked = true;
            pictureBox4.Image = Properties.Resources.difficulty_easy;
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            AboutBox1 aboutBox = new AboutBox1();
            aboutBox.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnHighScore_Click(object sender, EventArgs e)
        {
            lbHighScore.Text = Properties.Settings.Default.trackScore.ToString();
            tabControl1.SelectedIndex = 1;
        }
                
        private void menu_Load(object sender, EventArgs e)
        {
            //Write code to play music here 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {            
                var i = MessageBox.Show("Reset Game?", "Reset", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (i.ToString() == "No")
                {
                    return;
                }
            Properties.Settings.Default.trackScore = 0;
            Properties.Settings.Default.Save();
            lbHighScore.Text = Properties.Settings.Default.trackScore.ToString();
        }

        public void disableAllTbox()
        {
            tbS_E.Visible = false;
            tbS_E.ResetText();
            tbS_C.Visible = false;
            tbS_C.ResetText();
            tbS_A.Visible = false;
            tbS_A.ResetText();
            tbS_F.Visible = false;
            tbS_F.ResetText();
            tbL_F.Visible = false;
            tbL_F.ResetText();
            tbL_D.Visible = false;
            tbL_D.ResetText();
            tbL_B.Visible = false;
            tbL_B.ResetText();
            tbL_G.Visible = false;
            tbL_G.ResetText();
            tbL_E.Visible = false;
            tbL_E.ResetText();
            bsL_G.Visible = false;
            bsL_G.ResetText();
            bsL_B.Visible = false;
            bsL_B.ResetText();
            bsL_D.Visible = false;
            bsL_D.ResetText();
            bsL_F.Visible = false;
            bsL_F.ResetText();
            bsL_A.Visible = false;
            bsL_A.ResetText();
            bsS_A.Visible = false;
            bsS_A.ResetText();
            bsS_C.Visible = false;
            bsS_C.ResetText();
            bsS_E.Visible = false;
            bsS_E.ResetText();
            bsS_G.Visible = false;
            bsS_G.ResetText();
        }

        public void endGame()
        {
            timer1.Stop();
            disableAllTbox();
            timer1.Enabled = false;
            lbCountD.Text = "Ready?";
            DataCol.countDown = DataCol.counterPoint;
            if (Player.score > Properties.Settings.Default.trackScore)
            {
                Properties.Settings.Default.trackScore = Player.score;
                Properties.Settings.Default.Save();
                newHighRecord.Visible = true;
            }
            Player.score = 0;
            btn_start.Visible = true;
            btn_stop.Visible = false;
            lbPos.Visible = false;
            lbNeg.Visible = false;
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            endGame();
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            DataCol.countDown = DataCol.counterPoint;
            lbPos.Text = "+" + DataCol.posPoint.ToString();
            lbNeg.Text = "-" + DataCol.negPoint.ToString();
            newHighRecord.Visible = false;
            btn_stop.Visible = true;
            btn_start.Visible = false;
            timer1.Enabled = true;
            lbScore.Text = Player.score.ToString();
            timer1.Start();
            startTest();
        }

        public void checkAns()
        {
            switch (DataCol.nameTB)
            {
                case "tbS_E":
                    if (tbS_E.Text == "E")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "tbS_C":
                    if (tbS_C.Text == "C")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "tbS_A":
                    if (tbS_A.Text == "A")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "tbS_F":
                    if (tbS_F.Text == "F")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "tbL_F":
                    if (tbL_F.Text == "F")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "tbL_D":
                    if (tbL_D.Text == "D")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "tbL_B":
                    if (tbL_B.Text == "B")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "tbL_G":
                    if (tbL_G.Text == "G")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "tbL_E":
                    if (tbL_E.Text == "E")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "bsL_G":
                    if (bsL_G.Text == "G")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "bsL_B":
                    if (bsL_B.Text == "B")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "bsL_D":
                    if (bsL_D.Text == "D")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "bsL_F":
                    if (bsL_F.Text == "F")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "bsL_A":
                    if (bsL_A.Text == "A")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "bsS_A":
                    if (bsS_A.Text == "A")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "bsS_C":
                    if (bsS_C.Text == "C")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "bsS_E":
                    if (bsS_E.Text == "E")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                case "bsS_G":
                    if (bsS_G.Text == "G")
                    {
                        Player.score++;
                        DataCol.countDown += DataCol.posPoint;
                        lbPos.Visible = true;
                    }
                    else
                    {
                        DataCol.countDown -= DataCol.negPoint;
                        lbNeg.Visible = true;
                    }
                    break;
                default:
                    MessageBox.Show("Something is wrong!");
                    break;
            }
            timer1.Stop();
            timer1.Start();
            startTest();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbNeg.Visible = false;
            lbPos.Visible = false;
            int remainSecs = --DataCol.countDown;
            if (remainSecs > 0)
            {
                lbCountD.Text = remainSecs.ToString();
            }
            else
            {
                endGame();
            }
        }

        private void tbS_E_TextChanged(object sender, EventArgs e)
        {
            if (tbS_E.Text != "")
            {
                checkAns();
            }
        }

        private void tbS_C_TextChanged(object sender, EventArgs e)
        {
            if (tbS_C.Text != "")
            {
                checkAns();
            }
        }

        private void tbS_A_TextChanged(object sender, EventArgs e)
        {
            if (tbS_A.Text != "")
            {
                checkAns();
            }
        }

        private void tbS_F_TextChanged(object sender, EventArgs e)
        {
            if (tbS_F.Text != "")
            {
                checkAns();
            }
        }

        private void tbL_F_TextChanged(object sender, EventArgs e)
        {
            if (tbL_F.Text != "")
            {
                checkAns();
            }
        }

        private void tbL_D_TextChanged(object sender, EventArgs e)
        {
            if (tbL_D.Text != "")
            {
                checkAns();
            }
        }

        private void tbL_B_TextChanged(object sender, EventArgs e)
        {
            if (tbL_B.Text != "")
            {
                checkAns();
            }
        }

        private void tbL_G_TextChanged(object sender, EventArgs e)
        {
            if (tbL_G.Text != "")
            {
                checkAns();
            }
        }

        private void tbL_E_TextChanged(object sender, EventArgs e)
        {
            if (tbL_E.Text != "")
            {
                checkAns();
            }
        }

        private void bsS_G_TextChanged(object sender, EventArgs e)
        {
            if (bsS_G.Text != "")
            {
                checkAns();
            }
        }

        private void bsS_E_TextChanged(object sender, EventArgs e)
        {
            if (bsS_E.Text != "")
            {
                checkAns();
            }
        }

        private void bsS_C_TextChanged(object sender, EventArgs e)
        {
            if (bsS_C.Text != "")
            {
                checkAns();
            }
        }

        private void bsS_A_TextChanged(object sender, EventArgs e)
        {
            if (bsS_A.Text != "")
            {
                checkAns();
            }
        }

        private void bsL_A_TextChanged(object sender, EventArgs e)
        {
            if (bsL_A.Text != "")
            {
                checkAns();
            }
        }

        private void bsL_F_TextChanged(object sender, EventArgs e)
        {
            if (bsL_F.Text != "")
            {
                checkAns();
            }
        }

        private void bsL_D_TextChanged(object sender, EventArgs e)
        {
            if (bsL_D.Text != "")
            {
                checkAns();
            }
        }

        private void bsL_B_TextChanged(object sender, EventArgs e)
        {
            if (bsL_B.Text != "")
            {
                checkAns();
            }
        }

        private void bsL_G_TextChanged(object sender, EventArgs e)
        {
            if (bsL_G.Text != "")
            {
                checkAns();
            }
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            endGame();
            tabControl1.SelectedIndex = 0;
        }

        private void btnOkName_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                DataCol.negPoint = 5;
                DataCol.posPoint = 10;
                DataCol.counterPoint = 20;
            }
            else if (radioButton2.Checked)
            {
                DataCol.negPoint = 10;
                DataCol.posPoint = 5;
                DataCol.counterPoint = 15;
            }
            else if (radioButton3.Checked)
            {
                DataCol.negPoint = 13;
                DataCol.posPoint = 3;
                DataCol.counterPoint = 10;
            }
            else
            {
                MessageBox.Show("Select the Difficulty Level! ", "Difficulty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            tabControl1.SelectedIndex = 2;
            lbNeg.Visible = false;
            lbPos.Visible = false;
            btn_stop.Visible = false;
            disableAllTbox();
            lbScore.Text = 0.ToString();
            lbCountD.Text = "Ready?";
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox4.Image = Properties.Resources.difficulty_easy;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox4.Image = Properties.Resources.difficulty_medium;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox4.Image = Properties.Resources.difficulty_hard;
        }
    }
}
